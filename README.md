## JS Template

This template includes:

*  Typescript template
*  Linting using TSLint
*  Automatic releases using semantic-release (ESLint Code Convention) and Gitlab CI / CD
*  For public registry: Publishing of packages, for usage add the following lines to package.json, add the npm publish module in .releaserc and provide a valid NPM token: 
     
```
"publishConfig": {
    "access": "public",
    "registry": "https://registry.npmjs.org/",
    "tag": "latest"
}
```

*  Automatic Unit tests using Mocha
*  Automatic documentation publishing using Gitlab CI / CD and a self written script which puts the docs in the docs folder to the wiki