declare namespace rwthapp{
    var languages:any;
    var waitingScreen:any;
    var frame:any;
    var connection:any;
    var navigator:any;
}
declare namespace Pit.Labs{
    var Config:any;
    var OAuth2:any;
}

interface JQuery {
    tab(options: any): JQuery;
    render(tamplateName: string, params:any):void;
    select2(options:any):void;
    select2(key:string,value:string):void;
    datepicker(options:any):void;
    dataTable: any;
    DataTable: any;
    select2(option:any):any;
    offset():any;
    datepicker(option: any):void;
    modal(option1?:any, option2?:any):void;
    render(templateName:string, parameters:any):void;
    tooltip(option:any):void;
}

interface JQueryStatic {
    handlebars(option1: any, option2?: any, option3?: any):any;
}

interface Date {
    addMinutes(minutes: number):Date;
    addHours(minutes: number):Date;
    addDays(minutes: number):Date;
    printTime():string;
    printDate():string;
}

interface NumberConstructor{
    MAX_SAFE_INTEGER:number;
    MIN_SAFE_INTEGER:number;
}

interface RegExp{
    exec(value:any):any;
}

interface JSON{
    find(param:any):any;
}

interface window{
    open(param: string, option?: string):void;
    location: string;
}