declare namespace rwthapp.localization {
    const getLocalizedString: (key: string) => any;
    const setLanguage: (lang: string | null) => void;
    const getLanguage: () => string;
    const handlebarsLangStringHelper: (key: string, fallbackString: string) => any;
    const registerLangStrings: (languageKey: string, languageStrings: object) => void;
}
