// Attributes of rwthapp need to be mentioned individually, because rwthapp.templates is required to register templates
declare namespace rwthapp {
    var connection: any;
    var langStrings: any;
    var frame: any;
    var navigator: any;
    var languages: any;
}

interface Navigator {
    userLanguage: string;
}