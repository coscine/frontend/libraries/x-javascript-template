param(
    $token
)

$remoteUrl = git config --get remote.origin.url
$remoteUrl = $remoteUrl.replace("git@", "")
$remoteUrl = $remoteUrl -replace "https(.*?)@",""
$remoteUrl = $remoteUrl.replace(":", "/")
$remoteUrl = $remoteUrl.replace(".git", ".wiki.git")

$temporaryFolderName = "publishDocsTempFolder"

git clone "https://gitlab-ci-token:$($token)@$($remoteUrl)" $temporaryFolderName
cd $temporaryFolderName
Remove-Item *
cp -r ../docs/* ./
git add . 
git commit -m "Docs: Documentation Update" 
git push